import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

public class ChatServer{

		private static HashMap<String,Socket> clients = new HashMap<>();
		private static int id;
	public static void main(String[] args){
		try(ServerSocket serverSocket = new ServerSocket(5000)){
			while(true){
				Socket socket = serverSocket.accept();
				id++;
				System.out.println("Client "+id+" Connected!");
			    new SendServer(socket);
	
			}
		}catch(IOException e){
			System.out.println("Server Exception : " + e);
		}
	}

	public HashMap<String,Socket> getHashMap(){
		return clients;
	}
}