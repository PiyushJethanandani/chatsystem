

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;

public class SendServer extends Thread{
	private Socket socket;
	private ChatServer cs;
	public SendServer(Socket socket){
		this.socket = socket;
		this.start();
	}
	@Override
	public void run(){
		try{

			BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			// ChatServer cs = new ChatServer();
			HashMap<String,Socket> hs = cs.getHashMap();
			String echoString = "";
			String client = input.readLine();
			hs.put(client,socket);

			do{

				echoString = input.readLine();
				System.out.println(echoString);
				int i = echoString.lastIndexOf("~");
				String message = echoString.substring(0,i);
				String username = echoString.substring(i+1);
				if(hs.containsKey(username)){
				PrintWriter output = new PrintWriter(hs.get(username).getOutputStream(), true);
					
					output.println(message);
				}
				else{
					System.out.println("No Such Client Found in the Map");
				}

			}while(!echoString.equalsIgnoreCase("exit"));

		}catch(IOException e){
			System.out.println("Error in Send Server "+ e.getMessage());
		}
	}
}