import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class Echoer extends Thread{
	Socket socket;
	public Echoer(Socket socket){
		this.socket = socket;
		this.start();
	}
	@Override
	public void run(){
		try{
			BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			String echoString = "";
			do{
				echoString = input.readLine();
				System.out.println(echoString);
				
			}while(!echoString.equalsIgnoreCase("exit"));
		}catch(IOException e){
			System.out.println("Exception in Echoer : " + e);
		}
	}
}