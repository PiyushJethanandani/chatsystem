

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.Scanner;

public class Echoer1 extends Thread{
	private Socket socket;
	public Echoer1(Socket socket){
		this.socket = socket;
		this.start();
	}
	@Override
	public void run(){
		try{
			PrintWriter output = new PrintWriter(socket.getOutputStream(), true);
			Scanner scanner = new Scanner(System.in);
			System.out.println("Enter Your Name");
			String username = scanner.nextLine();

			output.println(username);

			String echoString="";
			do{
			
				System.out.println("Enter any string to be sent to Client : ");
				echoString = scanner.nextLine();
		
			if(!echoString.equalsIgnoreCase("exit")){
				
				output.println(echoString);					
			}
			
			}while(!echoString.equalsIgnoreCase("exit"));
		}catch(IOException e){
			System.out.println("Client Exception : " + e);
		}
		catch(Exception e){System.out.println("Some Exception occured in Echoer1" + e.getMessage());}
	}
}